// main.c
// AVR project

#include <avr/io.h>
#include <stdlib.h>
#include "strutil.h"

static char __buf[17];

#ifdef INTERNAL_STRLEN
static NONNULL __attribute__ ((pure)) uint8_t strlen(const  char *s) {
    register uint8_t r=0;
    while(s[r]) r++;
    return r;
}
#endif

NOINLINE NONNULL char * padded(char * buf, uint8_t width, const char pad) {

    register uint8_t x = strlen(buf);
    if (x >= width) return buf;
    buf[width] = 0;
    while(width) {
        width--;
        buf[width] = x ? buf[x-1] : pad;
        if (x) x--;
    }
    return buf;
}


NOINLINE NONNULL char * zero_padded(char *buf, uint8_t width) {
    return padded(buf, width, '0');
}

NOINLINE NONNULL char * zero_pad2(char *buf) {
    return padded(buf, 2, '0');
}

NOINLINE NONNULL char * zero_pad4(char *buf) {
    return padded(buf, 4, '0');
}

NOINLINE NONNULL char * zero_pad8(char *buf) {
    return padded(buf, 8, '0');
}

NOINLINE char * word2str(uint16_t b) {
    utoa(b, __buf, 10);
    return __buf;
}

NOINLINE char * word2hex(uint16_t b) {
    utoa(b, __buf, 16);
    return __buf;
}

NOINLINE char * int2str(int16_t b) {
    itoa(b, __buf, 10);
    return __buf;
}

NOINLINE char * word2bin(int16_t b) {
    utoa(b, __buf, 2);
    return __buf;
}

NOINLINE char * bcd2str(uint8_t bcd) {
    __buf[0] = (bcd >> 4) | '0';
    __buf[1] = (bcd & 0x0F) | '0';
    __buf[2] = 0;
    return __buf;
}


NOINLINE char * word2hex_pad2(uint16_t b) {
    word2hex(b);
    return zero_pad2(__buf);
}

NOINLINE char * word2hex_pad4(uint16_t b) {
    word2hex(b);
    return zero_pad4(__buf);
}

NOINLINE char * word2bin_pad8(int16_t b) {
    word2bin(b);
    return zero_pad8(__buf);
}
NOINLINE char * word2bin_pad16(int16_t b) {
    word2bin(b);
    return zero_padded(__buf, 16);
}
