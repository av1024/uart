#ifndef __STRUTIL_H__
#define __STRUTIL_H__

// Various string conversion tools
//!!!NOTE!!! number-to-str coinversion functions internally use utoa/itoa and single static buffer
//ALWAYS STORE COPY OF RESULT xxx2str/hex/bin functions before next call

//#include "gcc_macro.h"
#ifndef NOINLINE
    #define NOINLINE __attribute__((__noinline__))
    #define NONNULL __attribute__((__nonnull__))
#endif

// save 22 bytes if original strlen() not used
#define INTERNAL_STRLEN
//static uint8_t strlen(char *s);

char * word2str(uint16_t b);
char * int2str(int16_t b);

char * word2hex(uint16_t b);
char * word2hex_pad2(uint16_t b);
char * word2hex_pad4(uint16_t b);

char * word2bin(int16_t b);
char * word2bin_pad8(int16_t b);
char * word2bin_pad16(int16_t b);

char * bcd2str(uint8_t bcd);

// max 8 character internal buffer!!!
//const char * zero_pad(const char *str, uint8_t cnt);
char * padded(char * buf, uint8_t width, const char pad);
char * zero_padded(char *buf, uint8_t width);
char * zero_pad2(char *buf);
char * zero_pad4(char *buf);
char * zero_pad8(char *buf);

#endif  // __STRUTIL_H__
